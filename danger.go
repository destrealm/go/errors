// The functions defined in this file should not be used in ordinary use or they
// should be used with extreme care and consideration for possible outcomes.
// There are a narrowly defined series of use cases in which these functions can
// be helpful, but their use should be sparing and conservative.

package errors

// Consume the error returning the value without consideration for whatever
// might be happening upstream. Useful if you're uninterested in the error
// itself and already perform nil checks on the returned value.
func Consume(v interface{}, err error) interface{} {
	return v
}

// Must ensures that the error is nil; if it's not, it panics. This is likely
// the most useful of the functions defined here but beware that it disrupts
// program flow and should ONLY be used in circumstances where a panic makes
// sense (e.g. program flow cannot continue due to disrupted internal state).
func Must(v interface{}, err error) interface{} {
	if err != nil {
		panic(err)
	}
	return v
}
