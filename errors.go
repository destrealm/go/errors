package errors

import (
	"errors"
	"fmt"
	"hash/crc64"
	"strings"
)

// New is a convenience reference to errors.New. This avoids having to import
// this package with an alias.
var New = errors.New

// Errorf is a convenience reference to fmt.Errorf.
var Errorf = fmt.Errorf

// ErrorTracer defines a "traceable errors" interface. This interface combines
// the base Error implementation as well as an option for tracing the original
// error. This package also defines a few helper functions for comparing errors
// (notably Equal() and Is()). Be aware that this breaks certain expectations
// regarding how Go's errors should be handled and propogated but provides
// client code with the option to either a) match or handle the error returned
// by this package or b) drill down into the underlying cause (i.e. I know an
// encoding error happened, but what caused it?).
//
// I had written this before discovering pkg/errors, and it's possible this
// package may be deprecated when (or if) Go2 finally implements something
// similar.
type ErrorTracer interface {
	Add(string, interface{}) ErrorTracer
	Clone() ErrorTracer
	Code() int
	Do(error) ErrorTracer
	Equal(ErrorTracer) bool
	Error() string
	Get(string) (interface{}, bool)
	GetMeta() map[string]interface{}
	ReplaceMeta(map[string]interface{}) ErrorTracer
	SetCode(int) ErrorTracer
	Is(error) bool
	OriginalError() error
}

// Error container type. See ErrorTracer for a description of the interface.
type Error struct {
	err      string
	code     int
	original error
	metadata map[string]interface{}
}

// Clone the error creating a new instance.
//
// Be aware that this only creates a "shallow" clone. The internal metadata map
// is not duplicated between clones and is, in fact, shared. Modifications to
// one error's metadata will be reflected in all clones.
func Clone(err ErrorTracer) ErrorTracer {
	e, _ := err.(*Error)
	return &Error{
		err:      e.err,
		code:     e.code,
		original: e.original,
		metadata: e.metadata,
	}
}

// Guarantee returns an ErrorTracer regardless of the error's underlying type.
func Guarantee(err error) ErrorTracer {
	if e, ok := err.(ErrorTracer); ok {
		return e
	}

	s := ""
	if err != nil {
		s = err.Error()
	}

	return &Error{
		err:      s,
		original: nil,
		metadata: make(map[string]interface{}),
	}
}

// GuaranteeError ensures that the error `err` is of type `e`. If not, it will
// be wrapped and returned.
func GuaranteeError(err error, e ErrorTracer) ErrorTracer {
	if et, ok := err.(ErrorTracer); ok {
		if et.Is(e) {
			return et
		}
		return e.Do(et)
	}
	return e.Do(err)
}

// GuaranteeOrNil guarantees the return of an ErrorTracer or nil, depending on
// the original type of err.
func GuaranteeOrNil(err error) ErrorTracer {
	if err == nil {
		return nil
	}
	return Guarantee(err)
}

// Is compares whether err1 is of the same inherited base error as err2.
//
// There are some limitations with this sort of basic check. There's no
// reflection done to guarantee they're the same type, and only the error
// strings themselves are actually compared. This means that the comparison is
// fast but not necessarily accurate. For most projects, this should be
// sufficient.
//
// TODO: Migrate Error.Is to use this instead.
func Is(err1, err2 error) bool {
	return Guarantee(err1).Error() == Guarantee(err2).Error()
}

// Equal checks whether the two errors are equal to each other.
//
// As with Is() this function shares the same limitations and is a simplistic
// check that only validates the contents of both errors and their strings.
// However, this does go somewhat further by also checking the value of
// OriginalError().
//
// Note that this may fail somewhat spectacularly if it is used on errors that
// are wrapped multiple levels deep or if one of the arguments is nil. At
// present, there are no attempts to guard against either scenario.
//
// TODO: Fix this. Also migrate Error.Equal to use this instead.
func Equal(err1, err2 ErrorTracer) bool {
	return err1.Error() == err2.Error() && err1.OriginalError() == err2.OriginalError()
}

// DeepEqual uses Hash() to validate the "deep equality" of the errors. The
// errors are themselves traversed, hashed, and the hashes compared. Errors with
// identical error trees will be equal; others will not.
func DeepEqual(err1, err2 ErrorTracer) bool {
	return Hash(err1) == Hash(err2)
}

// Hash returns the CRC64 hash of the provided error tree.
//
// Presently, this only works with error trees using ErrorTracer. Eventually,
// support will be provided for stock Golang 1.13 error trees.
func Hash(err error) uint64 {
	hasher := crc64.New(crc64.MakeTable(crc64.ECMA))

	if err == nil {
		return 0
	}

	for i := 0; i < 10; i++ {
		if err == nil {
			break
		}

		if e, ok := err.(ErrorTracer); ok {
			hasher.Write([]byte(e.Error()))
			err = e.OriginalError()
		} else {
			hasher.Write([]byte(err.Error()))
			err = Guarantee(err)
		}
	}

	return hasher.Sum64()
}

// Unfurl returns a string containing the chain of events leading up to the
// final error.
func Unfurl(err error) string {
	s := "Error chain (initiating error last):\n"
	errs := make([]string, 0)

	for {
		if err == nil {
			break
		}
		errs = append(errs, err.Error())
		if e, ok := err.(ErrorTracer); ok {
			err = e.OriginalError()
		} else {
			break
		}
	}

	return fmt.Sprintf("%s -> %s", s, strings.Join(errs, "\n -> "))
}

// NewError creates a new Error instance with the specified error string. Start
// here when defining new traceable errors.
func NewError(err string) ErrorTracer {
	return &Error{
		err:      err,
		original: nil,
		metadata: make(map[string]interface{}),
	}
}

// NewErrorWithCode creates a new Error instance with the specified error
// message and code values pre-set.
func NewErrorWithCode(err string, code int) ErrorTracer {
	return &Error{
		err:      err,
		code:     code,
		original: nil,
		metadata: make(map[string]interface{}),
	}
}

// Add includes extra metadata with the error.
func (e *Error) Add(key string, value interface{}) ErrorTracer {
	e.metadata[key] = value
	return e
}

// Clone is functionally identical to the top-level function Clone() with the
// exception that it works on the current error directly, returning a clone of
// it.
func (e *Error) Clone() ErrorTracer {
	return Clone(e)
}

// Code returns the error code associated with this error.
func (e *Error) Code() int {
	return e.code
}

// Do the error. Simplistic, concise, and reasonably descriptive: Returns a copy
// of the traceable error encapsulating the `error` specified.
func (e *Error) Do(err error) ErrorTracer {
	return &Error{
		err:      e.err,
		original: err,
		metadata: make(map[string]interface{}),
	}
}

// Equal checks whether the two errors are equal to each other. Note that this
// is a relatively simple text and only compares string contents.
func (e *Error) Equal(err ErrorTracer) bool {
	return e.Error() == err.Error() && e.OriginalError() == err.OriginalError()
}

func (e *Error) DeepEqual(err ErrorTracer) bool {
	return DeepEqual(e, err)
}

// Error is included to satisfy the error interface.
func (e Error) Error() string {
	return e.err
}

// Get retrieves the specified key from this error's metadata and returns it in
// addition to a boolean value indicating whether the key exists. If the
// returned boolean is false, the key was not found.
func (e *Error) Get(key string) (interface{}, bool) {
	if v, ok := e.metadata[key]; ok {
		return v, true
	}
	return nil, false
}

// GetMeta returns extra metadata included with the error, if applicable.
func (e *Error) GetMeta() map[string]interface{} {
	return e.metadata
}

// ReplaceMeta replaces the internal metadata map with the map provided.
func (e *Error) ReplaceMeta(meta map[string]interface{}) ErrorTracer {
	e.metadata = meta
	return e
}

// SetCode modifies the internal code value for the current error.
func (e *Error) SetCode(code int) ErrorTracer {
	e.code = code
	return e
}

// Is compares whether err is of the same inherited base error as the current
// error.
func (e *Error) Is(err error) bool {
	return Guarantee(e).Error() == Guarantee(err).Error()
}

// OriginalError returns the error that originally provoked this traceable
// error; this is the encapsulated error.
func (e *Error) OriginalError() error {
	return e.original
}
