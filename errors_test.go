package errors_test

import (
	"testing"

	"git.destrealm.org/go/errors"
)

func Test_DefinedErrorIsEqual(t *testing.T) {
	err2 := errors.New("testError2")
	err3 := errors.New("testError3")

	testError := errors.NewError("test error")
	testError2 := testError.Do(err2)
	testError3 := testError.Do(err3)
	testError4 := testError.Do(err2)

	if !testError2.Equal(testError4) {
		t.Error("error2 should be equal to error4")
	}

	if testError2.Equal(testError3) {
		t.Error("error2 should not be equal to error3")
	}

	if !testError.Is(testError) {
		t.Error("error types should be equal (testError == testError)")
	}

	if !testError2.Is(testError) {
		t.Error("error types should be equal (testError2 == testError)")
	}

	if !testError3.Is(testError) {
		t.Error("error types should be equal (testError3 == testError)")
	}

	if !testError3.Is(testError) {
		t.Error("error types should be equal (testError3 == testError)")
	}
}

func Test_Clone(t *testing.T) {
	err := errors.NewError("this is a clone")
	if err.Code() != 0 {
		t.Errorf("expected an error code of 0 but got %d", err.Code())
	}

	clone := errors.Clone(err)
	clone.SetCode(500)

	if clone.Code() != 500 {
		t.Errorf("expected clone to contain an error code of 500 but got %d",
			clone.Code())
	}

	if err.Code() != 0 {
		t.Errorf("expected base error to still contain an error code of 0 but got %d",
			err.Code())
	}

	cl := clone.Clone()
	if cl.Code() != 500 {
		t.Errorf("clone-of-clone should contain an error of 500 but got %d",
			cl.Code())
	}
}

func Test_Guarantee(t *testing.T) {
	err := errors.New("testError")
	etErr := errors.NewError("ErrorTracer")

	tracer := errors.Guarantee(err)
	if tracer.Error() != "testError" {
		t.Errorf("expected error string \"testError\", got \"%s\"",
			tracer.Error())
	}

	tracer = errors.Guarantee(etErr)
	if tracer.Error() != "ErrorTracer" {
		t.Errorf("expected error string \"ErrorTracer\", got \"%s\"",
			tracer.Error())
	}

}

func Test_GuaranteeError(t *testing.T) {
	err := errors.New("testError")
	et1 := errors.NewError("ErrorTracer1")
	et2 := errors.NewError("ErrorTracer2")

	if !errors.GuaranteeError(err, et1).Is(et1) {
		t.Error("wrapped error should be of type ErrorTracer1")
	}

	if !errors.GuaranteeError(et2, et1).Is(et1) {
		t.Error("wrapped error should be of type ErrorTracer1")
	}

	if !errors.GuaranteeError(et1, et1).Is(et1) {
		t.Error("self-wrapped error should be of the same type, ErrorTracer1")
	}
}

func Test_NilGuaranteeError(t *testing.T) {
	var err error
	errtest := errors.NewError("test error")

	if errors.Guarantee(err).Is(errtest) {
		t.Error("Guarantee on nil should not trigger .Is()")
	}
}

func Test_Metadata(t *testing.T) {
	ErrInvalidCode := errors.NewError("invalid code received")

	e := ErrInvalidCode.Do(errors.Errorf("received %v", 400)).Add("code", 400)

	if !errors.Guarantee(e).Is(ErrInvalidCode) {
		t.Error("expected error type to be of ErrInvalidCode")
		return
	}

	if errors.Guarantee(e).OriginalError().Error() != "received 400" {
		t.Errorf(`error string should be "received 400" but got "%s"`, e.Error())
	}

	if val, ok := e.Get("code"); ok {
		if i, ok := val.(int); ok && i != 400 {
			t.Errorf(`received code of "%d" but expected 400`, i)
		}
	}

	if val, ok := e.GetMeta()["code"]; ok {
		if i, ok := val.(int); ok && i != 400 {
			t.Errorf(`received code of "%d" but expected 400`, i)
		}
	}
}

func Test_ReplaceMeta(t *testing.T) {
	ErrTest := errors.NewError("this is a test")
	ErrTest.Add("testing", "sample meta")

	if val, ok := ErrTest.Get("testing"); ok {
		if v, ok := val.(string); ok && v != "sample meta" {
			t.Errorf(`saved meta value was "%s" but expected "%s"`, v,
				"sample meta")
		}
	} else {
		t.Errorf(`unexpected meta value (not string): %v`, val)
	}

	m := map[string]interface{}{
		"testing": "not sample meta",
	}

	ErrTest.ReplaceMeta(m)

	if val, ok := ErrTest.Get("testing"); ok {
		if v, ok := val.(string); ok && v != "not sample meta" {
			t.Errorf(`replaced meta value was "%s" but expected "%s"`, v,
				"not sample meta")
		}
	} else {
		t.Errorf(`unexpected meta value (not string): %v`, val)
	}
}

func Test_Unfurl(t *testing.T) {
	ErrOne := errors.NewError("one")
	ErrTwo := errors.NewError("two")
	ErrThree := errors.NewError("three")

	e := ErrTwo.Do(ErrThree)
	e = ErrOne.Do(e)

	if errors.Unfurl(e) != "Error chain (initiating error last):\n -> one\n -> two\n -> three" {
		t.Error(`unfurl output not as expected`)
	}
}

func Test_Hasher(t *testing.T) {
	ErrOne := errors.NewError("one")
	ErrTwo := errors.NewError("two")

	if errors.Hash(ErrOne) == 0 {
		t.Error("hash should not be 0")
	}

	if errors.Hash(ErrTwo) == 0 {
		t.Error("hash should not be 0")
	}

	if errors.Hash(ErrOne) == errors.Hash(ErrTwo) {
		t.Error("hashes should not be equal")
	}

	e1 := ErrOne.Do(errors.New("one"))
	e2 := ErrTwo.Do(errors.New("two"))

	if errors.Hash(ErrOne) == errors.Hash(e1) {
		t.Error("derived errors should not have equivalent hashes")
	}

	if errors.Hash(ErrOne) == errors.Hash(e2) {
		t.Error("derived errors should not have equivalent hashes")
	}
}

func Test_DeepEqual(t *testing.T) {
	ErrOne := errors.NewError("one")

	e1 := ErrOne.Do(errors.New("test"))
	e2 := ErrOne.Do(errors.New("test"))

	if !errors.DeepEqual(e1, e2) {
		t.Error("errors should be deeply equal")
	}
}
